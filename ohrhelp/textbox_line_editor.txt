This is the text box line editor.

Use the arrow keys to move the cursor, and CTRL+Left or CTRL+Right to move by word.

Type to edit the lines.

The special codes listed at the bottom of the screen can be used to embed variable words/numbers into the text.

Press CTRL+SPACE to choose special characters to insert at the current cursor location.

CTRL+C Copies the current line.

CTRL+V Pastes the previously copied line, replacing the line that the cursor is on.
