This is the menu item detail editor. This is where you define what a menu item actually does.

*Caption - The caption is the text that will appear for this item in the menu. If you leave it blank it will be set to a default value.

*Type & Subtype - There are 5 different types of menu item.

  *Caption - Caption menu items just
   display the caption. There are
   two subtypes, selectable and
   unselectable. Selectable captions
   are only used to change tags or
   to close the menu. Unselectable
   captions are used for decoration
   or information.

  *Special Screen - This provides
   access to all the standard
   built-in special menus.

    *Items
    *Spells
    *Status
    *Equip
    *Order
    *Team
    *Order/Team (by general bitset)
    *Map (if allowed)
    *Save (if alowed)
    *Load
    *Quit
    *Volume
    *Map (even if not allowed)
    *Save (even if not allowed)
    *Margins (if supported by the
             current gfx backend)
    *Fullscreen (if playing windowed,
                 and it is supported)
    *Windowed (if playing fullscreen,
               and it is supported)

  *Go To Menu - This menu item will      jump to another menu when a player
   selects it. Use the subtype to
   select which menu.

  *Show Text Box - Cause a text box
   to be displayed. Use subtype to
   select which box. Note that if the
   menu has the "allow gameplay &
   scripts" bitset turned OFF then
   the text box will not appear until
   after the menu has closed. You may
   want to use the "Close menu if
   selected" menu item bitset to
   avoid that problem. Also note that
   if the "allow gameplay & scripts"
   bitset is turned ON, the text box
   will appear layered underneath
   the open menu.

  *Run script - Press ENTER on the
   subtype to browse a list of
   scripts. Note that as with text
   boxes, if the "allow gameplay &
   scripts" bitset is OFF then the
   script will not start until the
   menu has closed.

*Enable if Tag - Press ENTER or SPACE to browse a list of tags. If you specify one or more tags here, they will be checked before the menu item is enabled.  Press - to toggle between requiring a tag to be ON or OFF.

*Set Tag - This tag will be turned ON or OFF when the menu item is used. Press ENTER or SPACE to browse tags. Press - to toggle between turning a tag ON and turning it OFF.

*Toggle Tag - This tag will be switch OFF if it is ON and ON if it is OFF whenever this menu item is used. Press ENTER or SPACE to browse tags.

*Bitsets - See the help screen in this submenu for details.

*Extra Data - There are 3 pieces of extra data that you can attach to each menu item. These are just numbers. They don't mean anything unless you use them for your own special purposes from a plotscript. They are passed to triggered scripts as arguments if the menu bitset "Allow gameplay & scripts" is off.
