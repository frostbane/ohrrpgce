Welcome to the OHRRPGCE!

You can press F1 on any menu to see a help screen like this one.

Use the up and down arrow keys to scroll through long help screens.

This is the help for the main menu. Use the Up and Down arrow keys (or PgUp, PgDn, Home and End) to navigate through the menu. Press ENTER or SPACE to choose a menu option. Remember that you can press ESC to exit any submenu and get back to the previous menu. Pressing ESC on the main menu allows you to save (or not) and quit.

On this menu and some others you can also select a menu option by typing part of the menu option or pressing a letter repeatedly. For example typing "it" jumps to Edit Items.

You can press F5 on this menu to immediately reimport the previously compiled scripts; see the Script Management help file.
