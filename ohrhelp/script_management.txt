The script management menu allows you to access some tasks and tools related to your Plotscripts. (Check the documentation wiki at http://HamsterRepublic.com/ohrrpgce/ if you want to learn more about plotscripting)

* "Compile and/or Import plotscripts
   (.hss/.hs)"
  Imports scripts. The .hsi file for
  your game is first exported, then
  the selected scripts file (.hss
  extension recommended, .txt can
  also be used) is compiled with
  the HSpeak compiler. It's also
  possible to compile your scripts
  with HSpeak yourself and import
  the resulting .hs file.
  Importing overwrites any existing
  scripts - you must put them all in
  the same file (or 'include' them
  all in the same file) and import
  them all at once.

* "Export names for scripts (.hsi)"
  creates or updates a list of names
  (constants) that you can use while
  writing your scripts. Normally you
  don't need to manually export the
  .hsi file, as it's exported
  automatically when importing
  .hss/.txt files.

* "Check where scripts are used"
  produces a list of all the places
  where your game triggers a script.

* "Find broken script triggers"
  searches for references to scripts
  that have been renamed or removed.

You can press F5 on this menu or on Custom's main menu to immediately recompile and reimport the last script file that you imported. (Note that scripts aren't reimported automatically by the Game runner when using `Test Game' live-previewing; you will have to press F5 in-game to reload scripts.)
