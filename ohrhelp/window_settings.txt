This menu lets you set the in-game resolution and the window size.
Tip: You can change these settings while using the Test Game option to see the effects live!

Default window size: This lets you adjust roughly how large your game window appears on the screen by default. Depending on the graphics backend in use the player may also be able to change the window size themselves (this is the case on Windows and Macintosh). The scaling/zoom factor for the window (how large a single pixel appears) is then selected to get as close to your setting as possible.
You can choose "Maximize", which will maximise the window and put black bars around the outside if the graphics backend supports it, or as large as possible otherwise.
Note that the size is a percentage width or height of the screen, not area. 10% screen width and height is only 1% the screen area!

Test-Game window size: This setting is used instead of "Default window size" when using the Test Game option, so that you can set a more convenient smaller window size.

Display Width/Height: This is the size of the screen in pixels as seen from inside your game, not the actual size that the window will appear on your monitor. For example, if set to 320x200 (the default), then that means you should draw your backdrops 320x200 pixels in size.

Press ESC to go back.
