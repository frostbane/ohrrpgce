This is the enemy spawning menu.

Use the Up and Down arrow keys (or PgUp, PgDn, Home, and End) to navigate the menu.

Use the Left and Right arrow keys to change values, or you can type in numbers.

Hold ALT to display the name of the enemy that you are editing. While you are holding ALT you can press the left and right arrow keys to change which enemy you are editing, or you can type in an enemy ID number.

How Many to Spawn: This number determines how many enemies will be aded each time a sspawning event occurs (You can't spaawn more enemies than there are empty slots in the battle formation).

Spawn on Death: An enemy who will be spawned when this enemy dies.

on Non-Elemental Death: An enemy who will be spawned if the enemy is killed by an attack that does not have any elemental bitsets turned on.

Spawn When Alone: An enemy who will be spawned whenever the enemy is the last enemy alive in a battle formation.

Spawn on Non-elemental hit: An enemy who will be spawned each time the enemy is hit by an attack with no elemental bitsets turned on.

Spawn on (Elemental) Attack: For each of the elemental types, an enemy who will be spawned each time this enemy is hit with an attack that has the corresponding elemental bitset turned on.
