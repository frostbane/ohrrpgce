Choose a hand position on the sprite. This will be matched to the "handle position" defined for weapons in the Item Editor when the hero attacks in battle.

Use the arrow keys to move the flashing cursor. Press ENTER or SPACE to confirm the position.
