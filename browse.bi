'OHRRPGCE - File Browser
'
'Please read LICENSE.txt for GPL License details and disclaimer of liability
'See README.txt for code docs and apologies for crappyness of this code ;)

#ifndef BROWSE_BI
#define BROWSE_BI

DECLARE FUNCTION browse (byval special as integer, default as string, fmask as string, tmp as string, byref needf as integer = 0, helpkey as string) as string

#ENDIF
